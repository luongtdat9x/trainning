app.controller('BaoCaoTongHopToanQuocController',['$scope','BaoCaoTongHopToanQuocService', function ($scope,BaoCaoTongHopToanQuocService) {
          
    document.title = "Báo cáo Tổng hợp cấp quốc gia";
    
    $('.datepicker').datepicker({
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    
    $scope.search = {};
    var today = new Date();
    $scope.currentDate = moment(today).format('DD/MM/YYYY');
    $scope.objTuNgay = $scope.currentDate;
    $scope.objDenNgay = $scope.currentDate;
    $scope.search.vehicleType = "-1";
    $scope.paramReport = {};
    $scope.errorTuNgay = false;
    $scope.errorDenNgay = false;
    $scope.errorVehicleType = false;
    $scope.errorMessage = "";
          
    $scope.loaiHinhs = loaiHinhs;
          
    $scope.totalItems = 0;
    $scope.listResults = [];
    $scope.totalItemsPrint = 0;
    $scope.listResultsPrint = [];
    
    $scope.find = function () {
        if (!$scope.checkInput()) {
            return;
        }
        jQuery('#content_body').showLoading();
        return BaoCaoTongHopToanQuocService.getView(({"mapParam":$scope.search})).then(function (data) {   
            if(data.msg.errCode == "0"){
                $scope.jsonObject = JSON.parse(data.msg.object);
                $scope.totalItems = $scope.jsonObject.records;
                $scope.listResults = $scope.jsonObject.data;
            }
            jQuery('#content_body').hideLoading();
        },function (error) {
            checkError(error);
        });
    };  
    
    $scope.checkInput = function() {
        $scope.errorTuNgay = false;
        $scope.errorDenNgay = false;
        $scope.errorVehicleType = false;
        $scope.errorMessage = "";
        
        if ($scope.objTuNgay) {
            $scope.search.tuNgay = moment($scope.objTuNgay, "DD/MM/YYYY").valueOf();
        } else {
            $scope.errorTuNgay = true;
            $scope.errorMessage = "Bạn chưa chọn từ ngày!";
            $("#tuNgay").focus();
            return false;            
        }
        if ($scope.objDenNgay) {
            $scope.search.denNgay = moment($scope.objDenNgay, "DD/MM/YYYY").valueOf() + 86399000;
        } else {
            $scope.errorDenNgay = true;
            $scope.errorMessage = "Bạn chưa chọn đến ngày!";
            $("#denNgay").focus();
            return false;
        }
        if ($scope.objTuNgay > $scope.objDenNgay) {
            $scope.errorDenNgay = true;
            $scope.errorMessage = "Đến ngày phải lớn hơn từ ngày";
            $("#denNgay").focus();
            return false;
        }
        if (!$scope.search.vehicleType) {
            $scope.errorVehicleType = true;
            $scope.errorMessage = "Bạn chưa chọn loại hình!";
            $("#vehicleType").focus();
            return false;
        }
        return true;
    }
                
    $scope.printFile = function (type) {
        if (!$scope.checkInput()) {
            return;
        }
        jQuery('#content_body').showLoading();
        return BaoCaoTongHopToanQuocService.getFile(({"mapParam":$scope.search})).then(function (data) {   
            if(data.msg.errCode == "0"){
                var jsonObject = JSON.parse(data.msg.object);
                $scope.totalItemsPrint = jsonObject.records;
                $scope.listResultsPrint = jsonObject.data;
                var dateNow = new Date();
                var strDate = dateNow.getDate() + "_" + (dateNow.getMonth() + 1 < 10?"0" + (dateNow.getMonth() + 1):(dateNow.getMonth() + 1)) + "_" + dateNow.getFullYear();
                if($scope.listResultsPrint.length == 0) {
                    alert("Không tìm thấy bản ghi nào!");
                    jQuery('#content_body').hideLoading();
                    return;
                }
                $scope.paramReport.vehicleType = $("#vehicleType option:selected").text();
                $scope.paramReport.tuNgay = $scope.objTuNgay;
                $scope.paramReport.denNgay = $scope.objDenNgay;
                
                if(type == 'excel') {
                    var dataBody = "";
                    for(var i = 0; i < $scope.listResultsPrint.length; i++) {
                        dataBody += "<tr height=21 style='mso-height-source:userset;height:auto'>" +
                                "<td height=21 class=xl643831 width=60 style='height:auto;border-top:none;width:45pt'>"+ (i + 1) +"</td>" +
                                "<td colspan=2 class=xl713831 width=240 style='border-left:none;width:180pt'>" + ($scope.listResultsPrint[i].department_name==null?"":$scope.listResultsPrint[i].department_name) + "</td>" +
                                "<td class=xl663831 width=120 style='border-top:none;border-left:none;width:90pt'>" + format($scope.listResultsPrint[i].vehicle) + "</td>" +
                                "<td class=xl663831 width=180 style='border-top:none;border-left:none;width:135pt'>" + format($scope.listResultsPrint[i].distance) + "</td>" +
                                "<td colspan=2 class=xl683831 width=120 style='border-left:none;width:90pt'>" + format($scope.listResultsPrint[i].speed_distance) + "</td>" +
                                "<td class=xl683831 width=120 style='border-top:none;border-left:none;width:90pt'>" + format($scope.listResultsPrint[i].speed_count) + "</td>" +
                                "<td class=xl683831 width=120 style='border-top:none;border-left:none;width:90pt'>" + format($scope.listResultsPrint[i].speed_rate) + "</td>" +
                                "<td colspan=2 class=xl693831 width=150 style='border-left:none;width:113pt'>" + $scope.stringToTime($scope.listResultsPrint[i].stop_duration) + "</td>" +
                                "<td class=xl663831 width=120 style='border-top:none;border-left:none;width:90pt'>" + format($scope.listResultsPrint[i].stop_count) + "</td>" +
                            "</tr>\n";
                    }
                    dataBody += "<tr height=22 style='mso-height-source:userset;height:auto'>" +
                            "<td height=22 class=xl653831 width=60 style='height:auto;border-top:none;width:45pt'>Tổng</td>" +
                            "<td colspan=2 class=xl653831 width=240 style='border-left:none;width:180pt'>&nbsp;</td>" +
                            "<td class=xl673831 width=120 style='border-top:none;border-left:none;width:90pt'>" + format(jsonObject.totalVehicles) + "</td>" +
                            "<td class=xl673831 width=180 style='border-top:none;border-left:none;width:135pt'>" + format(jsonObject.totalDistances) + "</td>" +
                            "<td colspan=2 class=xl673831 width=120 style='border-left:none;width:90pt'>" + format(jsonObject.totalSpeedDistances) + "</td>" +
                            "<td class=xl673831 width=120 style='border-top:none;border-left:none;width:90pt'>" + format(jsonObject.totalSpeedCounts) + "</td>" +
                            "<td class=xl673831 width=120 style='border-top:none;border-left:none;width:90pt'>" + format(jsonObject.totalSpeedRates) + "</td>" +
                            "<td colspan=2 class=xl673831 width=150 style='border-left:none;width:113pt'>" + $scope.stringToTime(jsonObject.totalStopDurations) + "</td>" +
                            "<td class=xl673831 width=120 style='border-top:none;border-left:none;width:90pt'>" + format(jsonObject.totalStopCounts) + "</td>" +
                        "</tr>";
                    $scope.paramReport.dataBody = dataBody;
                    $scope.exportToExcel('tablePrint', 'BaoCaoTongHopToanQuoc_' + strDate, $scope.paramReport);
                }
                if(type == 'pdf') {
                    $scope.paramReport.totalVehicles = jsonObject.totalVehicles;
                    $scope.paramReport.totalDistances = jsonObject.totalDistances;
                    $scope.paramReport.totalSpeedDistances = jsonObject.totalSpeedDistances;
                    $scope.paramReport.totalSpeedCounts = jsonObject.totalSpeedCounts;
                    $scope.paramReport.totalSpeedRates = jsonObject.totalSpeedRates;
                    $scope.paramReport.totalStopDurations = jsonObject.totalStopDurations;
                    $scope.paramReport.totalStopCounts = jsonObject.totalStopCounts;
                    $scope.exportToPdf("BaoCaoTongHopToanQuoc_" + strDate, $scope.paramReport);
                }
            }
            else {
                alert("Lỗi truy xuất dữ liệu!");
            }
            jQuery('#content_body').hideLoading();
        },function (error) {
            checkError(error);
        });  
    };
    
    $scope.exportToExcel = (function() {
        var uri = 'data:application/vnd.ms-excel;base64,',
            base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }
        return function(table, name, dataParam) {
            var headTable = "<html xmlns:o=\"urn:schemas-microsoft-com:office:office\"xmlns:x=\"urn:schemas-microsoft-com:office:excel\"xmlns=\"http://www.w3.org/TR/REC-html40\"><head><meta http-equiv=Content-Type content=\"text/html; charset=utf-8\"><meta name=ProgId content=Excel.Sheet><meta name=Generator content=\"Microsoft Excel 15\"><link rel=File-List href=\"BaoCaoTongHopCapQuocGia_files/filelist.xml\"><style id=\"BaoCaoTongHopCapQuocGia_3831_Styles\"><!--table{mso-displayed-decimal-separator:\"\.\";mso-displayed-thousand-separator:\"\,\";}.xl153831{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:Calibri;mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:General;text-align:general;vertical-align:bottom;mso-background-source:auto;mso-pattern:auto;white-space:nowrap;}.xl633831{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:12.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:\"Times New Roman\";mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:\"\@\";text-align:center;vertical-align:middle;border:.5pt solid darkgray;background:white;mso-pattern:black none;white-space:normal;mso-text-control:shrinktofit;}.xl643831{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:\"Times New Roman\";mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:\"\@\";text-align:center;vertical-align:middle;border:.5pt solid darkgray;background:white;mso-pattern:black none;white-space:normal;mso-text-control:shrinktofit;}.xl653831{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:red;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:\"Times New Roman\";mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:\"\@\";text-align:left;vertical-align:middle;border:.5pt solid darkgray;background:white;mso-pattern:black none;white-space:normal;mso-text-control:shrinktofit;}.xl663831{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:\"Times New Roman\";mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:General;text-align:right;vertical-align:middle;border:.5pt solid darkgray;background:white;mso-pattern:black none;white-space:normal;mso-text-control:shrinktofit;}.xl673831{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:red;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:\"Times New Roman\";mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:\"\@\";text-align:right;vertical-align:middle;border:.5pt solid darkgray;background:white;mso-pattern:black none;white-space:normal;mso-text-control:shrinktofit;}.xl683831{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:\"Times New Roman\";mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:0;text-align:right;vertical-align:middle;border:.5pt solid darkgray;background:white;mso-pattern:black none;white-space:normal;mso-text-control:shrinktofit;}.xl693831{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:\"Times New Roman\";mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:\"\@\";text-align:right;vertical-align:middle;border:.5pt solid darkgray;background:white;mso-pattern:black none;white-space:normal;mso-text-control:shrinktofit;}.xl703831{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:\"Times New Roman\";mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:General;text-align:left;vertical-align:middle;border:none;background:white;mso-pattern:black none;white-space:normal;mso-text-control:shrinktofit;}.xl713831{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:\"Times New Roman\";mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:\"General Date\";text-align:left;vertical-align:middle;border:.5pt solid darkgray;background:white;mso-pattern:black none;white-space:normal;mso-text-control:shrinktofit;}.xl723831{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:11.0pt;font-weight:400;font-style:normal;text-decoration:none;font-family:\"Times New Roman\";mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:General;text-align:left;vertical-align:middle;border:none;background:white;mso-pattern:black none;white-space:normal;mso-text-control:shrinktofit;}.xl733831{padding-top:1px;padding-right:1px;padding-left:1px;mso-ignore:padding;color:black;font-size:16.0pt;font-weight:700;font-style:normal;text-decoration:none;font-family:\"Times New Roman\";mso-generic-font-family:auto;mso-font-charset:0;mso-number-format:General;text-align:center;vertical-align:middle;border:none;background:white;mso-pattern:black none;white-space:normal;mso-text-control:shrinktofit;}--></style></head><body><!--[if !excel]>&nbsp;&nbsp;<![endif]--><!--The following information was generated by Microsoft Excel's Publish as WebPage wizard.--><!--If the same item is republished from Excel, all information between the DIVtags will be replaced.--><!-----------------------------><!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD --><!-----------------------------><div id=\"BaoCaoTongHopCapQuocGia_3831\" align=center x:publishsource=\"Excel\"><table border=0 cellpadding=0 cellspacing=0 width=1230 style='border-collapse:collapse;table-layout:fixed;width:923pt'><col width=60 style='mso-width-source:userset;mso-width-alt:2194;width:45pt'><col width=145 style='mso-width-source:userset;mso-width-alt:5302;width:109pt'><col width=95 style='mso-width-source:userset;mso-width-alt:3474;width:71pt'><col width=120 style='mso-width-source:userset;mso-width-alt:4388;width:90pt'><col width=180 style='mso-width-source:userset;mso-width-alt:6582;width:135pt'><col width=15 style='mso-width-source:userset;mso-width-alt:548;width:11pt'><col width=105 style='mso-width-source:userset;mso-width-alt:3840;width:79pt'><col width=120 span=2 style='mso-width-source:userset;mso-width-alt:4388;width:90pt'><col width=65 style='mso-width-source:userset;mso-width-alt:2377;width:49pt'><col width=85 style='mso-width-source:userset;mso-width-alt:3108;width:64pt'><col width=120 style='mso-width-source:userset;mso-width-alt:4388;width:90pt'><tr height=30 style='mso-height-source:userset;height:22.5pt'><td colspan=12 height=30 class=xl723831 width=1230 style='height:22.5pt;width:923pt'><span style='mso-spacerun:yes'>        </span>BỘ GIAO THÔNG VẬN TẢI</td></tr><tr height=30 style='mso-height-source:userset;height:22.5pt'><td colspan=12 height=30 class=xl703831 width=1230 style='height:22.5pt;width:923pt'>TỔNG CỤC ĐƯỜNG BỘ VIỆT NAM</td></tr><tr height=50 style='mso-height-source:userset;height:37.5pt'><td colspan=12 height=50 class=xl733831 width=1230 style='height:37.5pt;width:923pt'>BÁO CÁO TỔNG HỢP CẤP QUỐC GIA</td></tr><tr height=30 style='mso-height-source:userset;height:22.5pt'><td height=30 class=xl153831 style='height:22.5pt'></td><td class=xl153831></td>" +
            "<td colspan=4 class=xl703831 width=410 style='width:307pt'>Loại hình:<spanstyle='mso-spacerun:yes'>  </span>"+ dataParam.vehicleType +"<spanstyle='mso-spacerun:yes'> </span></td><td class=xl153831></td><td class=xl153831></td><td class=xl153831></td><td class=xl153831></td><td class=xl153831></td><td class=xl153831></td></tr><tr height=30 style='mso-height-source:userset;height:22.5pt'><td height=30 class=xl153831 style='height:22.5pt'></td><td class=xl153831></td>" +
            "<td colspan=4 class=xl703831 width=410 style='width:307pt'>Từ ngày: "+ dataParam.tuNgay +"</td>" +
            "<td colspan=4 class=xl703831 width=410 style='width:308pt'>Đến ngày: "+ dataParam.denNgay +"</td><td class=xl153831></td><td class=xl153831></td></tr><tr height=1 style='mso-height-source:userset;height:.75pt'><td height=1 class=xl153831 style='height:.75pt'></td><td class=xl153831></td><td class=xl153831></td><td class=xl153831></td><td class=xl153831></td><td class=xl153831></td><td class=xl153831></td><td class=xl153831></td><td class=xl153831></td><td class=xl153831></td><td class=xl153831></td><td class=xl153831></td></tr><tr height=22 style='mso-height-source:userset;height:16.5pt'><td rowspan=2 height=44 class=xl633831 width=60 style='height:33.0pt;width:45pt'>STT</td><td colspan=2 rowspan=2 class=xl633831 width=240 style='width:180pt'>Sở GTVT</td><td rowspan=2 class=xl633831 width=120 style='width:90pt'>Số xe</td><td rowspan=2 class=xl633831 width=180 style='width:135pt'>Tổng km hành trình</td><td colspan=4 class=xl633831 width=360 style='border-left:none;width:270pt'>Vi phạm tốc độ</td><td colspan=3 class=xl633831 width=270 style='border-left:none;width:203pt'>Dừng/Đỗ</td></tr><tr height=22 style='mso-height-source:userset;height:16.5pt'><td colspan=2 height=22 class=xl633831 width=120 style='height:16.5pt;border-left:none;width:90pt'>Km</td><td class=xl633831 width=120 style='border-top:none;border-left:none;width:90pt'>Lần</td><td class=xl633831 width=120 style='border-top:none;border-left:none;width:90pt'>/1000 Km</td><td colspan=2 class=xl633831 width=150 style='border-left:none;width:113pt'>Giờ</td><td class=xl633831 width=120 style='border-top:none;border-left:none;width:90pt'>Lần</td></tr>";
            var footTable = "<![if supportMisalignedColumns]><tr height=0 style='display:none'><td width=60 style='width:45pt'></td><td width=145 style='width:109pt'></td><td width=95 style='width:71pt'></td><td width=120 style='width:90pt'></td><td width=180 style='width:135pt'></td><td width=15 style='width:11pt'></td><td width=105 style='width:79pt'></td><td width=120 style='width:90pt'></td><td width=120 style='width:90pt'></td><td width=65 style='width:49pt'></td><td width=85 style='width:64pt'></td><td width=120 style='width:90pt'></td></tr><![endif]></table></div></body></html>";
            table = headTable + dataParam.dataBody + footTable;
            var source = uri + base64(table);
            saveAs(source, name + '.xls');
        }
    })()
    
    $scope.exportToPdf = function(name, dataParam) {
        if($scope.totalItemsPrint == 0) return;
        var docDefinition = {
            pageSize: {
                width: 891,
                height: 630
            },
            pageOrientation: 'landscape',
            content: [{
                text: 'BỘ GIAO THÔNG VẬN TẢI',
                style: 'subheader3'
            }, {
                text: 'TỔNG CỤC ĐƯỜNG BỘ VIỆT NAM',
                style: 'subheader2',
                alignment: 'left'
            }, {
                text: 'BÁO CÁO TỔNG HỢP CẤP QUỐC GIA',
                style: 'subheader',
                alignment: 'center'
            }, {
                style: 'tableExample2',
                table: {
                    widths: ['25%', '25%', '25%', '25%'],
                    body: [
                        ['', {text: "Loại hình: "+ dataParam.vehicleType, bold: true, fontSize: 9, alignment: 'left'}, '', ''],
                        ['', {text: "Từ ngày: "+ dataParam.tuNgay, bold: true, fontSize: 9, alignment: 'left'}, {text: "Đến ngày: "+ dataParam.denNgay, bold: true, fontSize: 9, alignment: 'left'}, '']
                    ]
                },
                layout: 'noBorders'
            }, {
                style: 'tableExample',
                table: {
                    headerRows: 2,
                    widths: [50, 100, 90, '*', 90, 60, 60, 60, 60],
                    dontBreakRows: true,
                    // keepWithHeaderRows: 1,
                    body: [
                        [{
                            text: 'STT',
                            rowSpan: 2,
                            style: 'tableHeader',
                            alignment: 'center',
                            margin: [0, 7, 0, 7]
                        }, {
                            text: 'Sở GTVT',
                            rowSpan: 2,
                            style: 'tableHeader',
                            alignment: 'center',
                            margin: [0, 7, 0, 7]
                        }, {
                            text: 'Số xe',
                            rowSpan: 2,
                            style: 'tableHeader',
                            alignment: 'center',
                            margin: [0, 7, 0, 7]
                        }, {
                            text: 'Tổng số km hành trình',
                            rowSpan: 2,
                            style: 'tableHeader',
                            alignment: 'center',
                            margin: [0, 7, 0, 7]
                        }, {
                            text: 'Vi phạm tốc độ',
                            colSpan: 3,
                            style: 'tableHeader',
                            alignment: 'center',
                            margin: [0, 0, 0, 0]
                        }, {}, {}, {
                            text: 'Dừng/Đỗ',
                            colSpan: 2,
                            style: 'tableHeader',
                            alignment: 'center',
                            margin: [0, 0, 0, 0]
                        }, ''],
                        ['', '', '', '', {
                            text: 'Km',
                            style: 'tableHeader',
                            alignment: 'center',
                            margin: [0, 0, 0, 0]
                        }, {
                            text: 'Lần',
                            style: 'tableHeader',
                            alignment: 'center',
                            margin: [0, 0, 0, 0]
                        }, {
                            text: '/1000km',
                            style: 'tableHeader',
                            alignment: 'center',
                            margin: [0, 0, 0, 0]
                        }, {
                            text: 'Giờ',
                            style: 'tableHeader',
                            alignment: 'center',
                            margin: [0, 0, 0, 0]
                        }, {
                            text: 'Lần',
                            style: 'tableHeader',
                            alignment: 'center',
                            margin: [0, 0, 0, 0]
                        }]
                    ]
                }
            }],
            styles: {
                subheader: {
                    fontSize: 14,
                    bold: true,
                    margin: [0, 10, 0, 5]
                },
                tableExample2: {
                    width: 891,
                    margin: [0, 10, 0, 0]
                },
                tableExample: {
                    width: 891,
                    margin: [0, 10, 0, 10]
                },
                tableHeader: {
                    bold: true,
                    fontSize: 9,
                    color: 'black'
                },
                subheader2: {
                    fontSize: 10,
                    bold: true
                },
                subheader3: {
                    fontSize: 10,
                    margin: [20, 0, 0, 0]
                }
            },
            footer: function (currentPage, pageCount) {
                return { text: currentPage.toString() + ' / ' + pageCount, fontSize: 9, alignment: 'center', style: 'normalText', margin: [0, 10, 0, 0] }
            },
            defaultStyle: {
                // alignment: 'justify'
            }
        };
        for(var i = 0; i < $scope.listResultsPrint.length; i++) {
            var objLine = [];
            objLine.push({
                text: i + 1, fontSize: 9,
                alignment: 'center'
            });
            objLine.push({
                text: $scope.listResultsPrint[i].department_name, fontSize: 9,
                alignment: 'left'
            });
            objLine.push({
                text: format($scope.listResultsPrint[i].vehicle), fontSize: 9,
                alignment: 'right'
            });
            objLine.push({
                text: format($scope.listResultsPrint[i].distance), fontSize: 9,
                alignment: 'right'
            });
            objLine.push({
                text: format($scope.listResultsPrint[i].speed_distance), fontSize: 9,
                alignment: 'right'
            });
            objLine.push({
                text: format($scope.listResultsPrint[i].speed_count), fontSize: 9,
                alignment: 'right'
            });
            objLine.push({
                text: format($scope.listResultsPrint[i].speed_rate), fontSize: 9,
                alignment: 'right'
            });
            objLine.push({
                text: $scope.stringToTime($scope.listResultsPrint[i].stop_duration), fontSize: 9,
                alignment: 'right'
            });
            objLine.push({
                text: format($scope.listResultsPrint[i].stop_count), fontSize: 9,
                alignment: 'right'
            });
            docDefinition.content[4].table.body.push(objLine);
        }
        var objLine = [];
        objLine.push({
            text: "Tổng", fontSize: 9,
            alignment: 'left',
            color: 'red'
        });
        objLine.push({});
        objLine.push({
            text: format(dataParam.totalVehicles), fontSize: 9,
            alignment: 'right',
            color: 'red'
        });
        objLine.push({
            text: format(dataParam.totalDistances), fontSize: 9,
            alignment: 'right',
            color: 'red'
        });
        objLine.push({
            text: format(dataParam.totalSpeedDistances), fontSize: 9,
            alignment: 'right',
            color: 'red'
        });
        objLine.push({
            text: format(dataParam.totalSpeedCounts), fontSize: 9,
            alignment: 'right',
            color: 'red'
        });
        objLine.push({
            text: format(dataParam.totalSpeedRates), fontSize: 9,
            alignment: 'right',
            color: 'red'
        });
        objLine.push({
            text: $scope.stringToTime(dataParam.totalStopDurations), fontSize: 9,
            alignment: 'right',
            color: 'red'
        });
        objLine.push({
            text: format(dataParam.totalStopCounts), fontSize: 9,
            alignment: 'right',
            color: 'red'
        });
        docDefinition.content[4].table.body.push(objLine);
        pdfMake.createPdf(docDefinition).download(name + '.pdf');;
    }
    
    $scope.thoat = function() {
      window.location.href = "main";
    };  
    
    $scope.clearMes = function() {
        $scope.errorTuNgay = false;
        $scope.errorDenNgay = false;
        $scope.errorVehicleType = false;
        $scope.errorMessage = "";
    };  
    
    $scope.stringToNumberJS = function(filteredInput) {
        if(!filteredInput) return;
        if(filteredInput * 1 == 0) return 0;
        filteredInput = filteredInput.toString().replace(".", "#");
        filteredInput = filteredInput.replace(/\,/g, '.');
        filteredInput = filteredInput.replace("#", ",");
        return filteredInput;
    };
    
    $scope.stringToTime = function(str) {
        var seconds = str * 1;
        if(!seconds) return "0:00:00";
        var hh = parseInt(seconds/3600);
        seconds = seconds - hh * 3600;
        var mm = parseInt(seconds/60);
        seconds = seconds - mm * 60;
        var ss = parseInt(seconds);
        return hh + ":" + (mm < 10?"0"+mm:mm) + ":" + (ss < 10?"0"+ss:ss);
    };
    
//    $scope.chuyenTrang = function(namePage, deptId) {
//        setCookie("strDeptId", deptId, 30);
//        setCookie("strYear", $scope.search.year, 30);
//        setCookie("strMonth", $scope.search.month, 30);
//        setCookie("strVehicleType", $scope.search.vehicleType, 30);
//        setCookie("boolBCTH_ToanQuoc", 1, 30);
//        var strUrl = window.location.pathname;
//        var arrUrl = strUrl.split("\/");
//        if(namePage == 'sogtvt') {
//            window.location.href = "/" + arrUrl[1] + "/baocao-tonghop-sogtvt";
//        }
//        if(namePage == 'cdtdsogtvt') {
//            window.location.href = "/" + arrUrl[1] + "/baocao-chuyendetocdo-sogtvt";
//        }
//    };  
   
}]);