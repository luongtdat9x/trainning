'use strict';
var app = angular.module('MyApp', [ 'ngSanitize', 'ui.bootstrap','ngAnimate',"cfp.hotkeys"]);
app.run(function ($rootScope) {
    
});

app.config(configure);

function configure($httpProvider) {
    const afToken = angular.element('input[id="__AntiForgeryToken"]').attr('value');

    $httpProvider.defaults.headers.get = { 'CSRF-Token': afToken }; // only added for GET
    $httpProvider.defaults.headers.put = { 'CSRF-Token': afToken }; // added for PUT
    $httpProvider.defaults.headers.post = { 'CSRF-Token': afToken }; // added for POST

    // for some reason if we do the above we have to set the default content type for all 
    // looks like angular clears it when we add our own headers
    $httpProvider.defaults.headers.common = { "Content-Type": "application/json" };

}

app.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}]);
//-----------------Focus cho trư�?ng đầu tiên mỗi màn hình--------------
app.directive('ngFocus', function($timeout) {
    return {
        link: function ( scope, element, attrs ) {
            scope.$watch( attrs.ngFocus, function ( val ) {
                if ( angular.isDefined( val ) && val ) {
                    $timeout( function () { element[0].focus(); } );
                }
            }, true);
            
            element.bind('blur', function () {
                if ( angular.isDefined( attrs.ngFocusLost ) ) {
                    scope.$apply( attrs.ngFocusLost );
                    
                }
            });
        }
    };
});

//app.directive('checklistModel', ['$parse', '$compile', function($parse, $compile) {
//  // contains
// return function contains(arr, item, comparator) {
//    if (angular.isArray(arr)) {
//      for (var i = arr.length; i--;) {
//        if (comparator(arr[i], item)) {
//          return true;
//        }
//      }
//    }
//    return false;
//  };
//  });
 
//----------------End Focus cho trư�?ng đầu tiên mỗi màn hình-----------
app.directive('selectWatcher', function ($timeout) {
    return {
        link: function (scope, element, attr) {
            var last = attr.last;
            if (last === "true") {
                $timeout(function () {
                    $(element).parent().selectpicker('val', $(element).value);
                    $(element).parent().selectpicker('refresh');
                });
            }
        }
    };
});
$('input[disabled]').attr('tabindex', '-1');
	app.directive('enter',function(){
		return function(scope,element,attrs){
			element.bind("keydown",function(event){
				if(event.which===13){
					event.preventDefault();

					var fields=$(this).parents('form:eq(0),body').find(':input:visible');
					var index=fields.index(this);
          for(var i = index+1; i < fields.length-1; i++){
           if(!fields.eq(i).is(':disabled')){
                    fields.eq(i).focus();
                    return false;
                 }
            }

				}
			});
		};
	});
  
//----------------Prevent input min -  max value for year input--------
app.directive("preventTypingGreater", function() {
  return {
    link: function(scope, element, attributes) {
      var oldVal = null;
      element.on("keydown keyup", function(e) {
    if (Number(element.val()) > Number(attributes.max) &&
          e.keyCode != 46 // delete
          &&
          e.keyCode != 8 // backspace
        ) {
          e.preventDefault();
        //  element.val(oldVal);   // set value for this tag -- dungv 
        } else {
          oldVal = Number(element.val());
        }
      });
    }
  };
});
//----------------Prevent input min -  max value for year input--------


app.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;

            ctrl.$formatters.unshift(function (a) {
                elem[0].value = ctrl.$modelValue
                elem.priceFormat({
                  prefix: '',
                  centsSeparator: ',',
                  thousandsSeparator: '.'
                });
                
                return elem[0].value;
                //return $filter(attrs.format)(ctrl.$modelValue);
            });
//            elem.bind('blur', function(event) {
//                var plainNumber = elem.val();
//                elem.val($filter(attrs.format)(plainNumber));
//            });
            ctrl.$parsers.unshift(function(viewValue) {
              elem.priceFormat({
                prefix: '',
                centsSeparator: ',',
                thousandsSeparator: '.'
              });
              
              return elem[0].value;
          });
        }
    };
}]);

app.filter('dispNumber', function ($filter) {
      return function(input,fractionSize) {
        if (isNaN(input)||input==null) {
          return '0';
        } else {
          return formatNumberFromStr(input,fractionSize);
        }
      };
});

app.filter('dispTime', function($filter) {
    return function(input, format) {
        if (isNaN(input) || input == null) {
            return null;
        } else {
            return moment.duration(input, 'seconds').format(format, {
                trim: false
            });
        }
    };
});

(function($) {
  $.fn.priceFormat = function(options) {
    
    var defaults = {
      prefix: 'US$ ',
      suffix: '',
      centsSeparator: '.',
      thousandsSeparator: ',',
      limit: false,
      centsLimit: 2,
      clearPrefix: false,
      clearSufix: false,
      allowNegative: false,
      insertPlusSign: false
    };
    
    var options = $.extend(defaults, options);
    return this.each(function() {
      var obj = $(this);
      var is_number = /[0-9]/;
      var prefix = options.prefix;
      var suffix = options.suffix;
      var centsSeparator = options.centsSeparator;
      var thousandsSeparator = options.thousandsSeparator;
      var limit = options.limit;
      var centsLimit = options.centsLimit;
      var clearPrefix = options.clearPrefix;
      var clearSuffix = options.clearSuffix;
      var allowNegative = options.allowNegative;
      var insertPlusSign = options.insertPlusSign;
      if (insertPlusSign) allowNegative = true;
      var char_;
      function to_numbers(str) {
        var formatted = '';
        for (var i = 0; i < (str.length); i++) {
          char_ = str.charAt(i);
          if (formatted.length == 0 && char_ == 0) char_ = false;
          if (char_ && char_.match(is_number)) {
            if (limit) {
              if (formatted.length < limit) formatted = formatted + char_
            } else {
              formatted = formatted + char_
            }
          }
        }
        return formatted
      }
      
      function fill_with_zeroes(str) {
        while (str.length < (centsLimit + 1)) str = '0' + str;
        return str
      }
      
      function price_format(str) {
        var formatted = fill_with_zeroes(to_numbers(str));
        var thousandsFormatted = '';
        var thousandsCount = 0;
        if (centsLimit == 0) {
          centsSeparator = "";
          centsVal = ""
        }
        var centsVal = formatted.substr(formatted.length - centsLimit, centsLimit);
        var integerVal = formatted.substr(0, formatted.length - centsLimit);
        formatted = (centsLimit == 0) ? integerVal : integerVal + centsSeparator + centsVal;
        if (thousandsSeparator || $.trim(thousandsSeparator) != "") {
          for (var j = integerVal.length; j > 0; j--) {
            char_ = integerVal.substr(j - 1, 1);
            thousandsCount++;
            if (thousandsCount % 3 == 0) char_ = thousandsSeparator + char_;
            thousandsFormatted = char_ + thousandsFormatted
          }
          if (thousandsFormatted.substr(0, 1) == thousandsSeparator) thousandsFormatted = thousandsFormatted.substring(1, thousandsFormatted.length);
          formatted = (centsLimit == 0) ? thousandsFormatted : thousandsFormatted + centsSeparator + centsVal
        }
        if (allowNegative && (integerVal != 0 || centsVal != 0)) {
          if (str.indexOf('-') != -1 && str.indexOf('+') < str.indexOf('-')) {
            formatted = '-' + formatted
          } else {
            if (!insertPlusSign) formatted = '' + formatted;
            else formatted = '+' + formatted
          }
        }
        if (prefix) formatted = prefix + formatted;
        if (suffix) formatted = formatted + suffix;
        return formatted
      }
      //alert('priceFormat');
      function key_check(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        var typed = String.fromCharCode(code);
        var functional = false;
        var str = obj.val();
        var newValue = price_format(str + typed);
        if ((code >= 48 && code <= 57) || (code >= 96 && code <= 105)) functional = true;
        if (code == 8) functional = true;
        if (code == 9) functional = true;
   //     if (code == 13) functional = true;
        if (code == 46) functional = true;
        if (code == 37) functional = true;
        if (code == 39) functional = true;
        if (allowNegative && (code == 189 || code == 109)) functional = true;
        if (insertPlusSign && (code == 187 || code == 107)) functional = true;
        if (!functional) {
          e.preventDefault();
          e.stopPropagation();
          if (str != newValue) obj.val(newValue)
        }
      }

      function price_it() {
        var str = obj.val();
        var price = price_format(str);
        if (str != price) obj.val(price)
      }

      function add_prefix() {
        var val = obj.val();
        obj.val(prefix + val)
      }

      function add_suffix() {
        var val = obj.val();
        obj.val(val + suffix)
      }

      function clear_prefix() {
        if ($.trim(prefix) != '' && clearPrefix) {
          var array = obj.val().split(prefix);
          obj.val(array[1])
        }
      }

      function clear_suffix() {
        if ($.trim(suffix) != '' && clearSuffix) {
          var array = obj.val().split(suffix);
          obj.val(array[0])
        }
      }
      $(this).bind('keydown.price_format', key_check);
      $(this).bind('keyup.price_format', price_it);
      $(this).bind('focusout.price_format', price_it);
      if (clearPrefix) {
        $(this).bind('focusout.price_format', function() {
          clear_prefix()
        });
        $(this).bind('focusin.price_format', function() {
          add_prefix()
        })
      }
      if (clearSuffix) {
        $(this).bind('focusout.price_format', function() {
          clear_suffix()
        });
        $(this).bind('focusin.price_format', function() {
          add_suffix()
        })
      }
      if ($(this).val().length > 0) {
        price_it();
        clear_prefix();
        clear_suffix()
      }
    })
  };
  $.fn.unpriceFormat = function() {
    return $(this).unbind(".price_format")
  };
  $.fn.unmask = function() {
    var field = $(this).val();
    var result = "";
    for (var f in field) {
      if (!isNaN(field[f]) || field[f] == "-") result += field[f]
    }
    
    return result
  } 
})(jQuery);