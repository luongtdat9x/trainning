package action;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import javax.sql.DataSource;

public class AppServletContextListener implements ServletContextListener {
    protected ServletContext sc;

    public void contextInitialized(ServletContextEvent event) {
        sc = event.getServletContext();
        //Create tham so he thong ....
        //createDataSource(sc);
        createAppParam(sc);
    }

    public void contextDestroyed(ServletContextEvent event) {

    }

    protected String getDataSourceName() {
        return "jdbc/tpcpDS";
    }

    protected void createDataSource(ServletContext sc) {
        try {
            String dataSourceName = getDataSourceName();
            InitialContext ic = new InitialContext();
            DataSource ds = (DataSource)ic.lookup(dataSourceName);
        } catch (NamingException ex) {
            ex.printStackTrace();
        }

    }

    protected void createAppParam(ServletContext sc) {
        try {
        	
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    } 
}

