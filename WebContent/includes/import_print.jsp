<script src="styles/gsht/plugins/printFile/bootstrap.min.js"></script>
<script src="styles/gsht/plugins/printFile/pdfmake.min.js"></script>
<script src="styles/gsht/plugins/printFile/vfs_fonts.js"></script>
<script src="styles/gsht/plugins/printFile/FileSaver.js"></script>
<script>
    function format(x) {
        if(isNaN(x) || x == '' || x == null) return 0;
	n= x.toString().split('.');
        return n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".")+(n.length>1?","+n[1]:"");
    }
</script>