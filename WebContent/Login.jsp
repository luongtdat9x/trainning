<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>  
<html>
<head>
<title>Hệ thống giám sát hành trình - Tổng cục đường bộ Việt Nam</title>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type"></meta>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" type="text/css" href="styles/login/css/bootstrap-responsive.min.css" />
<script type="text/javascript" src="styles/login/js/bootstrap.min.js"></script>
<script type="text/javascript" src="styles/login/js/jquery.min.js"></script>
<link href="styles/login/css/stylesheet.css" rel="stylesheet" />
<link href="styles/login/css/stylesheets.css" rel="stylesheet" />
<link rel="shortcut icon" type="image/x-icon" href="styles/login/images/favicon.ico" />
<style>
        .header-top {
            background: url("styles/login/images/header_line_login.png") repeat-x scroll top left;
        }

        .contaner {
            background: url("styles/login/images/bglogin.png") no-repeat scroll center top;
            background-size: 100% 100%;
            /*height: 656px;*/
            display: block;
            position: relative;
        }


        #page-login {
            background: #fff none repeat scroll 0 0;
            border-radius: 7px;
            box-shadow: 1px 1px 7px #000;
            padding: 0 0 20px;
            width: 340px;
            margin: 10% 0 0 20%;
        }

        body {
            margin: 0;
            padding: 0;
        }

        #footerlogin {
            margin-top: 197px;
        }

        .respon-img {
            display: block;
            width: 100%;
        }

        ul.logo-top-login {
            margin: 0;
            padding: 0 0 0 10px;
        }

            ul.logo-top-login li {
                float: left;
                width: 33.33%;
                background: url("styles/login/images/bg-li.png") no-repeat scroll right center;
                background-size: auto 50%;
                list-style: none;
            }

        .header-top-right, .header-top-left {
            float: left;
            width: 50%;
        }

        @media only screen and (min-width: 1170px) {
            .header-top-right p {
                color: #fff;
                font-family: arial;
                font-size: 140%;
                font-weight: bold;
                text-align: center;
                margin-top: 15px;
            }
        }

        @media only screen and (max-width: 1170px) {
            .header-top-right p {
                color: #fff;
                font-family: arial;
                font-size: 140%;
                font-weight: bold;
                text-align: center;
                margin-top: 15px;
            }
        }

        @media only screen and (max-width: 1024px) {
            .header-top-right p {
                color: #fff;
                font-family: arial;
                font-size: 100%;
                font-weight: bold;
                text-align: center;
                margin-top: 15px;
            }
        }

        @media only screen and (max-width: 767px) {
            .header-top-right p {
                color: #fff;
                font-family: arial;
                font-size: 80%;
                font-weight: bold;
                text-align: center;
                margin-top: 15px;
            }
        }

        @media only screen and (max-width: 345px) {
            .header-top-right p {
                color: #fff;
                font-family: arial;
                font-size: 60%;
                font-weight: bold;
                text-align: center;
                margin-top: 15px;
            }
        }

        #page-login {
            background: #fff none repeat scroll 0 0;
            width: 340px;
        }

        .form-input {
            height: 90px;
            padding: 20px 20px 10px 20px;
            width: 300px;
        }

        .input-bottom {
            display: block;
            padding: 0 20px;
            text-align: right;
        }

        .form-input input.textbox {
            border: 1px solid #d0d0d0;
            border-radius: 4px;
            padding: 5px 0;
            text-indent: 5px;
            width: 100%;
        }

        #page-login h3 {
            background: #0979e9 none repeat scroll 0 0;
            color: #fff;
            display: block;
            margin: 0;
            padding: 10px 0;
            text-align: center;
            text-transform: uppercase;
            border-radius: 7px 7px 0 0;
        }

        #cmdLogin {
            background: #ed2226 none repeat scroll 0 0;
            border-radius: 5px;
            margin-top: 0 !important;
            padding: 5px 10px;
            width: auto !important;
        }

        .center-login {
            background: none;
            height: auto;
        }

        #footerlogin {
            bottom: 5px;
            margin: 0;
            position: absolute;
            width: 100%;
        }
    </style>
    <script type="text/javascript">
        window.onload = function () {
			document.documentElement.style.overflow = 'hidden';  // firefox, chrome
			document.body.scroll = "no"; // ie only
			var heightwindow = jQuery(window).height();
			$("#contaner").css('height', heightwindow);
		}
    </script>
</head>
<body>
    <form method="post" action="loginAction" autocomplete="off" onsubmit="false">
        <div id="contaner" class="contaner">
            <div class="row-fluid header-top">
                <div class="header-top-left">
                    <ul class="clearfix logo-top-login">
                        <li>
                            <img src="styles/login/images/bogtvt.png" class="respon-img" /></li>
                        <li>
                            <img src="styles/login/images/drvnlogin.png" class="respon-img" /></li>
                        <li>
                            <img src="styles/login/images/ubatgtlogin.png" class="respon-img" /></li>
                    </ul>
                </div>
                <div class="header-top-right header-top-right">
                    <p>
                        HỆ THỐNG XỬ LÝ & KHAI THÁC SỬ DỤNG DỮ LIỆU TỪ TB.GSHT
                                <br>
                        THUỘC TỔNG CỤC ĐƯỜNG BỘ VIỆT NAM
                    </p>
                </div>
            </div>
            <div id="page-login" class="clearfix">
                <h3>Đăng nhập</h3>
                <div class="center-login">
                    <div class="form-input" style="height: 65px">
    
                        <input name="loginForm.ma_nsd" type="text" id="txtUsername" tabindex="1" class="textbox" placeholder="User Name" style="margin-bottom: 10px;" />
    
    
                        <input name="loginForm.mat_khau" type="password" id="txtPassword" tabindex="2" class="textbox" placeholder="Password" />
                    </div>
                    <div id="Div1" class="input-bottom">
                        <input type="submit" name="cmdLogin" value="Đăng nhập" id="cmdLogin" class="button" resourcekey="cmdLogin" style="outline: none;" />
    
                    </div>
                    <div class="clear"></div>
                    <div style="display: block">
                        <span id="lblError" style="color:Red;"></span>
                    </div>
                </div>
            </div>
    
            <div id="footerlogin">
                <p style="text-align: left; width: 45%; float: left; color: #ffffff; line-height: 17px; padding-top: 10px; margin: 0 0 0 1%;">
                    Địa chỉ liên hệ: Lô D20 - Khu Đô thị Cầu Giấy - Hà Nội 
                </p>
                <p style="text-align: right; float: right; width: 45%; color: #ffffff; margin: 0 1% 0 0; line-height: 17px; padding-top: 10px;">
                    Giải pháp của Hanel. Hotline: 0961 930 199
                </p>
            </div>
        </div>
    </form>
</body>
</html>