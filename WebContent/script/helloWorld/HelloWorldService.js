'use strict';
app.service('HelloWorldService', ['BaseServices', function (BaseServices) {

	var urls = {
		getList : "helloagl?action=list"
	}

	function getList(data) {
		return BaseServices.callAPI(urls.getList, 'POST', data);
	}
	
	var service = {
		getList : getList
	};
	
	return service;
}
]);