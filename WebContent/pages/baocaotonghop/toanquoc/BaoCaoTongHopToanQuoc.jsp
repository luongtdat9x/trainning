<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<jsp:include page="/includes/tpcp_header.jsp"></jsp:include>
<jsp:include page="/includes/import_print.jsp"></jsp:include>
<script src="script/baocaotonghop/toanquoc/BaoCaoTongHopToanQuocController.js"></script>
<script src="script/baocaotonghop/toanquoc/BaoCaoTongHopToanQuocService.js"></script>

<div class="content-wrapper" ng-controller="BaoCaoTongHopToanQuocController">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Báo cáo Tổng hợp toàn quốc
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i><a href="main"> Trang chủ</a></li>
        <li> Báo cáo</li>
        <li> Tổng hợp</li>
        <li class="active"> Toàn quốc</li>
      </ol>
    </section>
    <section class="content">
      <div class="callout callout-info">
        <h4>Thông báo hệ thống!</h4>
        Hệ thống đã hoàn thành việc tổng hợp các báo cáo vi phạm của các tháng tới tháng 10 năm 2019. Đề nghị các sở GTVT thực hiện khai thác dữ liệu và trích xuất báo cáo tổng hợp vi phạm để phục vụ công tác quản lý , chấn chỉnh, nhắc nhở và xử lý các trường hợp vi phạm theo quy định.
      </div>
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tiêu chí lựa chọn</h3>
            </div>
            <!-- /.box-header -->
            <form role="form">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label>Từ ngày</label>
                            <div class="input-group date">
                              <input id="tuNgay" ng-model="objTuNgay" class="form-control datepicker pull-right" ng-click="clearMes()">
                              <label class="input-group-addon" for="tuNgay">
                                <i class="fa fa-calendar"></i>
                              </label>
                            </div>
                            <div ng-show="errorTuNgay" class="text-danger">
                                <span class="error" style="color:red">{{errorMessage}}</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label>Đến ngày</label>
                            <div class="input-group date">
                              <input id="denNgay" ng-model="objDenNgay" class="form-control datepicker pull-right" ng-click="clearMes()">
                              <label class="input-group-addon" for="denNgay">
                                <i class="fa fa-calendar"></i>
                              </label>
                            </div>
                            <div ng-show="errorDenNgay" class="text-danger">
                                <span class="error" style="color:red">{{errorMessage}}</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label>Loại hình</label>
                            <select id="vehicleType" class="form-control" ng-model="search.vehicleType" ng-click="clearMes()">
                                <option ng-repeat="loaiHinh in loaiHinhs" value="{{loaiHinh.key}}">{{loaiHinh.value}}</option>
                            </select>
                            <div ng-show="errorVehicleType" class="text-danger">
                                <span class="error" style="color:red">{{errorMessage}}</span>
                            </div>
                          </div>
                        </div>
                    </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer text-center">
                    <button type="button" class="btn btn-primary" ng-click="find()">Xem báo cáo</button>
                    <button type="button" class="btn btn-primary" ng-click="printFile('excel')">Xuất Excel</button>
                    <button type="button" class="btn btn-primary" ng-click="printFile('pdf')">Xuất Pdf</button>
              </div>
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Báo cáo Tổng hợp cấp quốc gia</h3>
            </div>
            <!-- /.box-header -->
            <form role="form">
                <div class="box-body" id="page-print" style="padding: 0px;">
                    <table class="table table-bordered">
                      <thead id="headerTable">                  
                        <tr>
                            <th rowspan="2">STT</th>
                            <th rowspan="2">Sở GTVT</th>
                            <th rowspan="2">Số xe</th>
                            <th rowspan="2">Tổng km hành trình</th>
                            <th colspan="3">Vi phạm tốc độ</th>
                            <th colspan="2">Dừng đỗ</th>
                        </tr>
                        <tr>
                            <th>Km</th>
                            <th>Lần</th>
                            <th>/1000km</th>
                            <th>Giờ</th>
                            <th>Lần</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr ng-repeat="result in listResults">
                            <td class="text-center">{{$index + 1}}</td>
                            <td class="text-left"><a href="" ng-click="chuyenTrang('sogtvt', result.department_id)">{{result.department_name}}</a></td>
                            <td class="text-right">{{stringToNumberJS(result.vehicle | number:0)}}</td>
                            <td class="text-right">{{stringToNumberJS(result.distance | number:2)}}</td>
                            <td class="text-right"><a href="" ng-click="chuyenTrang('cdtdsogtvt', result.department_id)">{{stringToNumberJS(result.speed_distance | number:2)}}</a></td>
                            <td class="text-right"><a href="" ng-click="chuyenTrang('cdtdsogtvt', result.department_id)">{{stringToNumberJS(result.speed_count | number:0)}}</a></td>
                            <td class="text-right"><a href="" ng-click="chuyenTrang('cdtdsogtvt', result.department_id)">{{stringToNumberJS(result.speed_rate | number:0)}}</a></td>
                            <td class="text-right">{{stringToTime(result.stop_duration)}}</td>
                            <td class="text-right">{{stringToNumberJS(result.stop_count | number:0)}}</td>
                        </tr>
                        <tr ng-show="totalItems != 0" style="color: red;">
                            <td class="text-left">Tổng</td>
                            <td class="text-left">&nbsp;</td>
                            <td class="text-right">{{stringToNumberJS(jsonObject.totalVehicles | number:0)}}</td>
                            <td class="text-right">{{stringToNumberJS(jsonObject.totalDistances | number:2)}}</td>
                            <td class="text-right">{{stringToNumberJS(jsonObject.totalSpeedDistances | number:2)}}</td>
                            <td class="text-right">{{stringToNumberJS(jsonObject.totalSpeedCounts | number:0)}}</td>
                            <td class="text-right">{{stringToNumberJS(jsonObject.totalSpeedRates | number:0)}}</td>
                            <td class="text-right">{{stringToTime(jsonObject.totalStopDurations)}}</td>
                            <td class="text-right">{{stringToNumberJS(jsonObject.totalStopCounts | number:0)}}</td>
                        </tr>
                        <tr ng-show="totalItems == 0">
                            <td class="text-center" colspan="9" style="color: #C0C0C0">Không có dữ liệu</td>
                        </tr>
                      </tbody>
                    </table>
              </div>
              <!-- /.box-body -->
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
  </div>
<script src="/GSHT/styles/gsht/js/demo.js"></script>
<jsp:include page="/includes/tpcp_bottom.jsp"></jsp:include>