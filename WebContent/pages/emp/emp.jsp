<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<jsp:include page="/includes/tpcp_header.jsp"></jsp:include>
<jsp:include page="/includes/import_print.jsp"></jsp:include>
<script src="script/emp/EmpController.js"></script>
<script src="script/emp/EmpService.js"></script>

<div class="content-wrapper" ng-controller="EmpController">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quản lý nhân viên
      </h1>
      <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i><a href="main"> Trang chủ</a></li>
        <li> Quản lý</li>
        <li class="active"> Nhân viên</li>
      </ol>
    </section>
    <section class="content">
      <div class="callout callout-info">
        <h4>Thông báo hệ thống!</h4>
        Hệ thống đã hoàn thành việc tổng hợp các báo cáo vi phạm của các tháng tới tháng 10 năm 2019. Đề nghị các sở GTVT thực hiện khai thác dữ liệu và trích xuất báo cáo tổng hợp vi phạm để phục vụ công tác quản lý , chấn chỉnh, nhắc nhở và xử lý các trường hợp vi phạm theo quy định.
      </div>
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tiêu chí lựa chọn</h3>
            </div>
            <!-- /.box-header -->
            <form role="form">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label>Từ ngày</label>
                            <div class="input-group date">
                              <input id="tuNgay" ng-model="objTuNgay" class="form-control datepicker pull-right" ng-click="clearMes()">
                              <label class="input-group-addon" for="tuNgay">
                                <i class="fa fa-calendar"></i>
                              </label>
                            </div>
                            <div ng-show="errorTuNgay" class="text-danger">
                                <span class="error" style="color:red">{{errorMessage}}</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label>Đến ngày</label>
                            <div class="input-group date">
                              <input id="denNgay" ng-model="objDenNgay" class="form-control datepicker pull-right" ng-click="clearMes()">
                              <label class="input-group-addon" for="denNgay">
                                <i class="fa fa-calendar"></i>
                              </label>
                            </div>
                            <div ng-show="errorDenNgay" class="text-danger">
                                <span class="error" style="color:red">{{errorMessage}}</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label>Loại hình</label>
                            <select id="vehicleType" class="form-control" ng-model="search.vehicleType" ng-click="clearMes()">
                                <option ng-repeat="loaiHinh in loaiHinhs" value="{{loaiHinh.key}}">{{loaiHinh.value}}</option>
                            </select>
                            <div ng-show="errorVehicleType" class="text-danger">
                                <span class="error" style="color:red">{{errorMessage}}</span>
                            </div>
                          </div>
                        </div>
                    </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer text-center">
                    <button type="button" class="btn btn-primary" ng-click="find()">Xem báo cáo</button>
                    <button type="button" class="btn btn-primary" ng-click="printFile('excel')">Xuất Excel</button>
                    <button type="button" class="btn btn-primary" ng-click="printFile('pdf')">Xuất Pdf</button>
              </div>
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Danh sách nhân viên</h3>
            </div>
            <!-- /.box-header -->
            <form role="form">
                <div class="box-body" id="page-print" style="padding: 0px;">
                    <table class="table table-bordered">
                      <thead id="headerTable">                  
                        <tr>
                            <th>STT</th>
                            <th>Mã nhân viên</th>
                            <th>Tên nhân viên</th>
                            <th>Tên phòng ban</th>
                            <th>Công việc</th>
                            <th>Lương</th>
                            <th>Ngày tạo</th>
                            <th>Trạng thái</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr ng-repeat="result in listResults">
                            <td class="text-center">{{$index + 1}}</td>
                            <td class="text-right">{{result.manv}}</td>
                            <td class="text-right">{{result.tennv}}</td>
                            <td class="text-right">{{result.tenphongban}}</td>
                            <td class="text-right">{{result.congviec}}</td>
                            <td class="text-right">{{result.luong}}</td>
                            <td class="text-center">{{result.ngaytao}}</td>
                            <td class="text-left">{{result.trangthai}}</td>
                        </tr>
                        <tr ng-show="totalItems == 0">
                            <td class="text-center" colspan="9" style="color: #C0C0C0">Không có dữ liệu</td>
                        </tr>
                      </tbody>
                    </table>
              </div>
              <!-- /.box-body -->
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
  </div>
<script src="/GSHT/styles/gsht/js/demo.js"></script>
<jsp:include page="/includes/tpcp_bottom.jsp"></jsp:include>