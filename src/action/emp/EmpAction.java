package action.emp;

import java.io.PrintWriter;
import java.io.Reader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.opensymphony.xwork2.ActionSupport;

import action.AppAction;
import dao.Connect;
import delegate.emp.EmpDelegateNew;
import entities.Message;

public class EmpAction extends AppAction {
	public String executeAction(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		return SUCCESS;
	}

//	public String view(HttpServletRequest request,
//			HttpServletResponse response) throws Exception {
//		try {
//			msg = new Message();
//			EmpDelegate empDel = new EmpDelegate();
//			Map<String, Object> mapObj = new HashMap<String, Object>();
//			Collection<DbEmp> listEmp = empDel.getListEmp();
//			mapObj.put("listEmp", listEmp);
//			mapObj.put("totalItems", listEmp.size());
//			msg.setObject(mapObj);
//			msg.setErrCode("0");
//			msg.setErrMessage("Truy vấn dữ liệu thành công!");
//			return SUCCESS;
//		} catch (Exception ex) {
//			msg.setErrCode("1");
//			msg.setErrMessage("Truy vấn dữ liệu thất bại!");
//			throw ex;
//		}
//	}
	public String view(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
        SqlSession session = Connect.getSqlSessionFactory().openSession();
//		Reader reader = Resources.getResourceAsReader("SqlMapConfig.xml");
//		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
//		SqlSession session = sqlSessionFactory.openSession();
        if (session == null) {
            return ActionSupport.SUCCESS;
        }
		try {
			msg = new Message();
			EmpDelegateNew empDel = new EmpDelegateNew(session);
			Map<String, Object> mapObj = new HashMap<String, Object>();
			Collection<?> listEmp = empDel.getListEmp(null);
			mapObj.put("listEmp", listEmp);
			mapObj.put("totalItems", listEmp.size());
			msg.setObject(mapObj);
			msg.setErrCode("0");
			msg.setErrMessage("Truy vấn dữ liệu thành công!");
			return SUCCESS;
		} catch (Exception ex) {
			msg.setErrCode("1");
			msg.setErrMessage("Truy vấn dữ liệu thất bại!");
			throw ex;
		} finally {
			session.close();
		}
	}
	
	public String add(HttpServletRequest request,
			HttpServletResponse response) {
		response.setContentType("application/pdf");
        response.setHeader("Content-Disposition",
                           "attachment; filename=TestFile.pdf");
		CreatePDF.getFile(response);
		return ActionSupport.ERROR;
	}
}
