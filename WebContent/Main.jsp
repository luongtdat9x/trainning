<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<jsp:include page="/includes/tpcp_header.jsp"></jsp:include>
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Trang chủ
      </h1>
      <ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i> Trang chủ</li>
      </ol>
    </section>
    <section class="content">
      <div class="callout callout-info">
        <h4>Thông báo hệ thống!</h4>
        Hệ thống đã hoàn thành việc tổng hợp các báo cáo vi phạm của các tháng tới tháng 10 năm 2019. Đề nghị các sở GTVT thực hiện khai thác dữ liệu và trích xuất báo cáo tổng hợp vi phạm để phục vụ công tác quản lý , chấn chỉnh, nhắc nhở và xử lý các trường hợp vi phạm theo quy định.
      </div>
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-6">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tra cứu thống tin hành trình của phương tiện</h3>
            </div>
            <!-- /.box-header -->
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Biển kiểm soát</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Nhập biển kiểm soát">
                  </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Thời điểm</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-group date">
                                  <input type="text" class="form-control pull-right" id="datepicker">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div style="width: 45%;float: left;">
                                    <select class="form-control select2">
                                        <option value="00:00">00:00</option>
                                        <option value="23:59">23:59</option>
                                    </select>
                                </div>
                                <span style="width: 10%;float: left; text-align: center; line-height: 30px;">-</span> 
                                <div style="width: 45%;float: left;">
                                    <select class="form-control select2">
                                        <option value="00:00">00:00</option>
                                        <option value="23:59">23:59</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.input group -->
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Tìm kiếm</button>
              </div>
              <!-- /.box-footer -->
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-6">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Thông tin liên hệ</h3>
            </div>
            <div class="box-body">
              <strong><i class="fa fa-exclamation-circle margin-r-5"></i> Tổng cục đường bộ Việt Nam</strong>
              <p class="text-muted">
                  Hotline: <b>0936 295 919</b>
              </p>
              <strong><i class="fa fa-gear margin-r-5"></i> Hỗ trợ kỹ thuật</strong>
              <p class="text-muted">
                  Điện thoại: 0961 930 199<br/>
                  Email: gsht.drvn@gmail.com
              </p>
              <br/>
              <br/>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <!-- Left col -->
        <div class="col-md-6">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Thống kê phương tiện truyền dữ liệu theo loại hình</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <p class="text-muted">
                  Dữ liệu ngày 14/01/2020 (Có 645.510/1.246.114 phương tiện truyền dữ liệu ~ 51.8%)
              </p>
              <div id="container" style="width: 500px; height: 250px; margin: 0 auto"></div>
            </div>
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-6">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Thống kê vi phạm tốc độ</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <p class="text-muted">
                  Dữ liệu ngày 14/01/2020 (Có tổng số 15.404 lượt vị phạm tốc độ)
              </p>
              <div id="container2" style="width: 400px; height: 250px; margin: 0 auto"></div>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
  </div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
  $(function () {
      //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy'
    });
    //Initialize Select2 Elements
    $('.select2').select2();
    // Bieu do 1
    Highcharts.chart('container', {
      chart: {
        type: 'pie'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      plotOptions: {
        series: {
          dataLabels: {
            enabled: true,
            format: '{point.name}: {point.y:.1f}%'
          }
        }
      },

      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> của tổng số<br/>'
      },

      series: [
        {
          name: "Loại phương tiện",
          colorByPoint: true,
          data: [
            {
              name: "Xe chưa phân loại",
              y: 62.74
            },
            {
              name: "Xe đầu kéo",
              y: 10.57
            },
            {
              name: "Xe taxi tải",
              y: 7.23
            },
            {
              name: "Xe taxi",
              y: 5.58
            },
            {
              name: "Xe tuyến cố định",
              y: 4.02
            },
            {
              name: "Xe bus",
              y: 1.92
            },
            {
              name: "Xe du lịch",
              y: 1.92
            },
            {
              name: "Xe hợp đồng",
              y: 7.62
            },
            {
              name: "Xe container",
              y: 7.62
            }
          ]
        }
      ]
    });
    //Bieu do 2
    Highcharts.chart('container2', {
      chart: {
        type: 'pie'
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      plotOptions: {
        series: {
          dataLabels: {
            enabled: true,
            format: '{point.name}: {point.y:.1f}%'
          }
        }
      },

      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> của tổng số<br/>'
      },

      series: [
        {
          name: "Tốc độ vi phạm",
          colorByPoint: true,
          data: [
            {
              name: "Từ 5 - 10 km",
              y: 62.74,
              drilldown: "Từ 5 -10 km"
            },
            {
              name: "Từ 10 - 20 km",
              y: 10.57,
              drilldown: "Từ 10 - 20 km"
            },
            {
              name: "Từ 20 - 35 km",
              y: 7.23,
              drilldown: "Từ 20 - 35 km"
            }
          ]
        }
      ]
    });
  })
</script>
<%@ include file="/includes/tpcp_bottom.jsp"%>