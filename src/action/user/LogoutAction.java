package action.user;


import com.opensymphony.xwork2.ActionSupport;

import action.AppAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogoutAction extends AppAction {
    private static String SUCCESS = "success";

    protected String executeAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return ActionSupport.SUCCESS;
    }
}
