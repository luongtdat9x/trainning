<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="common.AppConstants"%>
<HTML ng-app='MyApp'>
  <HEAD>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Trang chủ</title> 
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="styles/gsht/plugins/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="styles/gsht/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="styles/gsht/plugins/Ionicons/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="styles/gsht/plugins/jvectormap/jquery-jvectormap.css">
    <link rel="stylesheet" href="styles/gsht/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="styles/gsht/plugins/select2/dist/css/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="styles/gsht/css/adminltenew.min.css">
    <link rel="stylesheet" href="styles/gsht/css/gsht.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="styles/gsht/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  
    <link rel="stylesheet" href="styles/gsht/css/showLoading.css">
    <script src="styles/gsht/plugins/moment/moment.js"></script>
    <script src="styles/gsht/js/moment-duration-format.min.js"></script>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- jQuery -->
    <script src="styles/gsht/plugins/jquery/dist/jquery.min.js"></script>
    <script src="styles/gsht/js/jquery-1.11.3.min.js"></script>
    <!-- Bootstrap -->
    <script src="styles/gsht/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="styles/gsht/plugins/fastclick/lib/fastclick.js"></script>
    <script src="styles/gsht/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="styles/gsht/plugins/select2/dist/js/select2.full.min.js"></script>
    <!-- AdminLTE -->
    <script src="styles/gsht/js/adminlte.js"></script>
    <!-- Angular js -->
    <script src="angularjs/v1.5.8/angular.js"></script>
    <script src="angularjs/v1.5.8/angular-animate.js"></script>
    <script src="angularjs/v1.5.8/angular-sanitize.js"></script>
    <script type="text/javascript" src="angularjs/angular.min.js"></script>
    <script type="text/javascript" src="angularjs/angular-cookies.js"></script>
    <script src="angularjs/ngStorage.min.js"></script>
    <script src="angularjs/ui-bootstrap-tpls-2.5.0.js"></script>
    <script src="angularjs/js/hotkeys.js"></script>
    <script src="styles/gsht/js/jquery.showLoading.js"></script>
    <script src="styles/gsht/js/download2.js"></script>
    <script src="script/common/common.js"></script>
    <script src="script/module.js"></script>
    <script src="script/BaseServices.js"></script>
    <link rel="stylesheet" href="styles/gsht/css/fixTable.css">
    <link rel="stylesheet" href="styles/gsht/css/stylePage.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="styles/login/images/favicon.ico" />
</HEAD>

<body class="hold-transition skin-blue sidebar-mini" id="content_body">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="/GSHT/main" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">GSHT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Hệ thống GSHT</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">1</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Bạn có 1 thông báo mới</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> Bạn vừa đổi mật khẩu
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">Xem tất cả</a></li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="styles/gsht/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Administrator</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="styles/gsht/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Administrator - Quản trị hệ thống
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Đổi mật khẩu</a>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Đăng xuất</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="styles/gsht/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Administartor</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Đang đăng nhập</a>
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active">
          <a href="/GSHT/main">
            <i class="fa fa-dashboard"></i> <span>Trang chủ</span>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-laptop"></i> <span>Giám sát trực tuyến</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
              <i class="fa fa-file-pdf-o"></i> <span class="title-nav">Báo cáo</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Tổng hợp
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="baocao-tonghop-toanquoc"><i class="fa fa-circle-o"></i> Toàn quốc</a></li>
                <li><a href="baocao-tonghop-sogtvt"><i class="fa fa-circle-o"></i> Sở GTVT</a></li>
                <li><a href="baocao-tonghop-theoloaihinh"><i class="fa fa-circle-o"></i> Loại hình</a></li>
                <li><a href="baocao-tonghop-theodonvivt"><i class="fa fa-circle-o"></i> Đơn vị vận tải</a></li>
                <li><a href="baocao-tonghop-theodvcungcap"><i class="fa fa-circle-o"></i> Đơn vị truyền DL</a></li>
                <li><a href="baocao-tonghop-ptvpthoigianlx"><i class="fa fa-circle-o"></i> Phương tiện vi phạm<br/>thời gian lái xe</a></li>
                <li><a href="baocao-tonghop-ptvptocdo"><i class="fa fa-circle-o"></i> Phương tiện vi phạm tốc<br/>độ 5 lần / 1000 km</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Chuyên đề tốc độ
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="baocao-chuyendetocdo-toanquoc"><i class="fa fa-circle-o"></i> Toàn quốc</a></li>
                <li><a href="baocao-chuyendetocdo-sogtvt"><i class="fa fa-circle-o"></i> Sở GTVT</a></li>
                <li><a href="baocao-chuyendetocdo-donvivantai"><i class="fa fa-circle-o"></i> Đơn vị vận tải</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Đơn vị truyền dữ liệu</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Truyền dữ liệu
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Toàn quốc</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Sở GTVT</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Đơn vị vận tải</a></li>
                <li><a href="baocao-truyendulieu-donvitruyendl"><i class="fa fa-circle-o"></i> Đơn vị truyền DL</a></li>
                <li><a href="baocao-truyendulieu-biensoxe"><i class="fa fa-circle-o"></i> Biển số xe</a></li>
                <li><a href="baocao-truyendulieu-donvimoitruyendl"><i class="fa fa-circle-o"></i> Phương tiện mới truyền<br/>dữ liệu</a></li>
                <li><a href="baocao-truyendulieu-khongtruyendvtruyen"><i class="fa fa-circle-o"></i> Phương tiện không truyền<br/>dữ liệu theo Đơn vị truyền DL</a></li>
                <li><a href="baocao-truyendulieu-khongtruyensogtvt"><i class="fa fa-circle-o"></i> Phương tiện không truyền<br/>dữ liệu theo sở GTVT</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Báo cáo thống kê không<br/>truyền dữ liệu</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Tấn suất truyền không<br/>đảm bảo</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Nhà cung cấp truyền<br/>song song</a></li>
                <li><a href="baocao-truyendulieu-chitiethanhtrinhadmin"><i class="fa fa-circle-o"></i> Chi tiết hành trình (admin)</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Phương tiện truyền sai<br/>dữ liệu theo đơn vị truyền dữ <br/>liệu</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Không truyền dữ liệu
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Sở GTVT</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Đơn vị vận tải</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Phương tiện</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Chi tiết
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="baocao-chitiet-hanhtrinh"><i class="fa fa-circle-o"></i> Hành trình</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Dừng đỗ</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Vi phạm tốc độ</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Vi phạm T/G lái xe liên tục</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Vi phạm tổng T/G lái xe</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Chi tiết bản tin lỗi</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-adjust"></i> <span class="title-nav">Nâng cao</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Cảnh báo làm sai lệch dữ liệu<br/>giám sát</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Báo cáo các hiện tượng bất <br/>thường của dữ liệu giám sát</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Báo cáo theo dõi không truyền<br/>dữ liệu khi xe đang chạy</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Báo cáo truyền sai dữ liệu<br/>tọa độ</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Báo cáo phương tiện truyền <br/>không đúng định dạng biển số</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Tra cứu lịch sử quản lý thông<br/>tin phương tiện</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Điều tra tai nạn</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Thống kê lưu lượng, vận tốc<br/>trên tuyến đường</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Quản lý dữ liệu biển báo</a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder-o"></i> <span class="title-nav">Danh mục</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Đơn vị vận tải</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Phương tiện</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Phương tiện chưa xác định</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Lái xe</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Danh sách tỉnh</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Đơn vị truyền DL</a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-gears"></i> <span class="title-nav">Quản trị hệ thống</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Danh sách người dùng</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Quản trị nhóm người dùng</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Quản trị role</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Quản trị quyền</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Quản lý thông báo</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Nhật ký truy cập</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Quản trị cấu hình tham số <br/>kết nối</a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-info-circle"></i> <span class="title-nav">Thông tin cần biết</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Hướng dẫn sử dụng</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Văn bản, thông tư</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Danh sách các đơn vị hợp quy </a>
            </li>
            <li>
                <a href="#"><i class="fa fa-circle-o"></i> Thông tin hỗ trợ</a>
            </li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
