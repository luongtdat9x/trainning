package dao;

import java.util.Collection;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

public abstract class AbDAO {
	protected SqlSession session;

	public AbDAO(SqlSession session) {
		this.session = session;
	}

	public Collection<?> getListByCondition(String sql,
			Map<String, Object> params) throws Exception {
		try {
			return session.selectList(sql, params);
		} catch (Exception ex) {
			throw ex;
		}
	}
	
	public Collection<?> getListByCondition(String sql,
			Object params) throws Exception {
		try {
			return session.selectList(sql, params);
		} catch (Exception ex) {
			throw ex;
		}
	}

	public Object getOneByCondition(String sql,
			Map<String, Object> params) throws Exception {
		try {
			return session.selectOne(sql, params);
		} catch (Exception ex) {
			throw ex;
		}
	}

	public Object getOneByCondition(String sql,
			Object params) throws Exception {
		try {
			return session.selectOne(sql, params);
		} catch (Exception ex) {
			throw ex;
		}
	}

	public int getCount(String sql,
			Map<String, Object> params) throws Exception {
		int cnt = 0;
		try {
			cnt = (Integer)session.selectOne(sql, params);
		} catch (Exception ex) {
			throw ex;
		}
		return cnt;
	}
	
	public int getCount(String sql,
			Object params) throws Exception {
		int cnt = 0;
		try {
			cnt = (Integer)session.selectOne(sql, params);
		} catch (Exception ex) {
			throw ex;
		}
		return cnt;
	}

	public long getSEQ(String sql,
			Map<String, Object> params) throws Exception {
		long cnt = 0;
		try {
			cnt = (Long)session.selectOne(sql, params);
		} catch (Exception ex) {
			throw ex;
		}
		return cnt;
	}


	public Object getById(String sql, int id) throws Exception {
		Object obj = null;
		try {
			obj = session.selectOne(sql, id);
		} catch (Exception ex) {
			throw ex;
		}
		return obj;
	}

	public int insert(String sql, Map<String, Object> params) throws Exception {
		int cnt = 0;
		try {
			cnt = session.insert(sql, params);
		} catch (Exception ex) {
			throw ex;
		}
		return cnt;
	}

	public int insert(String sql, Object obj) throws Exception {
		int cnt = 0;
		try {
			cnt = session.insert(sql, obj);

		} catch (Exception ex) {
			throw ex;
		}
		return cnt;
	}
	
	public int update(String sql, Map<String, Object> params) throws Exception {
		int cnt = 0;
		try {
			cnt = session.update(sql, params);
		} catch (Exception ex) {
			throw ex;
		}
		return cnt;
	}

	public int update(String sql, Object obj) throws Exception {
		int cnt = 0;
		try {
			cnt = session.update(sql, obj);
		} catch (Exception ex) {
			throw ex;
		}
		return cnt;
	}
	
	public int delete(SqlSession session, String sql,
			Map<String, Object> params) throws Exception {
		int cnt = 0;
		try {
			cnt = session.delete(sql, params);
		} catch (Exception ex) {
			throw ex;
		}
		return cnt;
	}
	
	public int delete(String sql, Object obj) throws Exception {
		int cnt = 0;
		try {
			cnt = session.delete(sql, obj);
		} catch (Exception ex) {
			throw ex;
		}
		return cnt;
	}
	
	public int delete(String sql, String ma) throws Exception {
		int cnt = 0;
		try {
			cnt = session.delete(sql, ma);
		} catch (Exception ex) {
			throw ex;
		}
		return cnt;
	}

	public int delete(String sql, int id) throws Exception {
		int cnt = 0;
		try {
			cnt = session.delete(sql, id);

		} catch (Exception ex) {
			throw ex;
		}
		return cnt;
	}	
}
