package delegate.emp;

import java.sql.SQLException;
import java.util.Collection;

import dao.emp.EmpDAO;
import entities.emp.DbEmp;

public class EmpDelegate {
	public Collection<DbEmp> getListEmp() throws SQLException {
		EmpDAO empDao = new EmpDAO();
		return empDao.getListEmp();
	}
}
