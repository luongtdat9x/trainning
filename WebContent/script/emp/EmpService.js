'use strict';
app.service('EmpService', ['BaseServices', function (BaseServices) {
var urls = {
        getView : "empagl?action=view",
        getFile : "empagl?action=process"
    }
function getView(data) {
      return BaseServices.callAPI(urls.getView, 'POST', data);
    }
function getFile(data) {
      return BaseServices.callAPI(urls.getFile, 'POST', data);
    }
var service = {
        getView : getView,
        getFile : getFile
    };return service;
}
]);