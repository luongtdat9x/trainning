package dao.emp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import common.ConnectionUtils;
import entities.emp.DbEmp;

public class EmpDAO {
	private static Connection conn = null;
	static {
		try {
			conn = ConnectionUtils.getConnection();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public Collection<DbEmp> getListEmp() throws SQLException {
		String sql = "SELECT ROW_NUMBER() OVER(ORDER BY DEV_EMPLOYEE.ID DESC) AS STT, "
				+ "DEV_EMPLOYEE.ID AS MANV, "
				+ "DEV_EMPLOYEE.FULLNAME AS TENNV, "
				+ "DEV_DEPARTMENT.ID AS MAPHONGBAN, "
				+ "DEV_DEPARTMENT.NAME AS TENPHONGBAN, "
				+ "DEV_EMPLOYEE.JOB AS CONGVIEC, "
				+ "DEV_EMPLOYEE.SALARY AS LUONG, "
				+ "to_char(DEV_EMPLOYEE.CREATE_DATE, 'dd/MM/yyyy') AS NGAYTAO, "
				+ "DEV_EMPLOYEE.STATUS AS TRANGTHAI "
				+ "FROM DEV_EMPLOYEE "
				+ "INNER JOIN DEV_DEPARTMENT ON DEV_EMPLOYEE.DEPT_ID = DEV_DEPARTMENT.ID";

		PreparedStatement pstm = conn.prepareStatement(sql);
		ResultSet rs = pstm.executeQuery();
		Collection<DbEmp> list = new ArrayList<DbEmp>();
		while (rs.next()) {
			DbEmp emp = new DbEmp();
			emp.setStt(rs.getString("STT"));
			emp.setManv(rs.getString("MANV"));
			emp.setTennv(rs.getString("TENNV"));
			emp.setMaphongban(rs.getString("MAPHONGBAN"));
			emp.setTenphongban(rs.getString("TENPHONGBAN"));
			emp.setCongviec(rs.getString("CONGVIEC"));
			emp.setLuong(rs.getString("LUONG"));
			emp.setNgaytao(rs.getString("NGAYTAO"));
			emp.setTrangthai(rs.getString("TRANGTHAI"));
			list.add(emp);
		}
		return list;
	}
}
