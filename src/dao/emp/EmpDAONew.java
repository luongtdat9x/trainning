package dao.emp;

import java.util.Collection;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import dao.AbDAO;

public class EmpDAONew extends AbDAO {
    public EmpDAONew(SqlSession session) {
        super(session);
    }
    public Collection<?> getListEmp(Map<String, Object> params) throws Exception {
        return getListByCondition("Emp.getListEmp", params);
    }
}
