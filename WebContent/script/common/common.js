function hash(msg) {
    // returns the byte length of an utf8 string
    var s = msg.length;
    for (var i = msg.length - 1; i >= 0; i--) {
        var code = msg.charCodeAt(i);
        if (code > 0x7f && code <= 0x7ff) s++;
        else if (code > 0x7ff && code <= 0xffff) s += 2;
        if (code >= 0xDC00 && code <= 0xDFFF) i--; //trail surrogate
    }

    return randomNum(s, 2);
}

function randomNum(value,num){
  return 1234+value+3247*num;
}

var loaiHinhs = [];
loaiHinhs.push({
    key: "-1",
    value: "Tất cả loại phương tiện "
});
loaiHinhs.push({
    key: "0",
    value: "Xe chưa phân loại"
});
loaiHinhs.push({
    key: "100",
    value: "Xe tuyến cố định"
});
loaiHinhs.push({
    key: "200",
    value: "Xe bus"
});
loaiHinhs.push({
    key: "300",
    value: "Xe hợp đồng"
});
loaiHinhs.push({
    key: "400",
    value: "Xe du lịch"
});
loaiHinhs.push({
    key: "500",
    value: "Xe Container"
});
loaiHinhs.push({
    key: "600",
    value: "Xe tải"
});
loaiHinhs.push({
    key: "700",
    value: "Xe taxi"
});
loaiHinhs.push({
    key: "800",
    value: "Xe taxi tải"
});
loaiHinhs.push({
    key: "900",
    value: "Xe đầu kéo"
});
//
//function HtmlToExcel(element, filename = '') {
//    var htmls = "";
//    var uri = 'data:application/vnd.ms-excel;base64,';
//    var template =
//        '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta charset="utf-8"><style>body {font-family: "Times New Roman";}</style></head><body>{table}</body></html>';
//    var base64 = function(s) {
//        return window.btoa(unescape(encodeURIComponent(s)))
//    };
//    var format = function(s, c) {
//        return s.replace(/{(\w+)}/g, function(m, p) {
//            return c[p];
//        })
//    };
//    htmls = document.getElementById(element).innerHTML;
//    var ctx = {
//        worksheet: 'Worksheet',
//        table: htmls
//    }
//    var link = document.createElement("a");
//    link.download = filename ? filename + '.xls' : 'document.xls';
//    link.href = uri + base64(format(template, ctx));
//    link.click();
//}

function HtmlToExcel(templateReport, strJsonReport, filename = 'document') {
    templateReport.mainContent = replaceTemplate(templateReport.mainContent, strJsonReport);
    var tableBody = "";
    var i = 1;
    strJsonReport.data.forEach(chiTietHanhTrinh => {
        chiTietHanhTrinh.stt = i;
        tableBody += replaceTemplate(templateReport.tableBody, chiTietHanhTrinh);
        i++;
    });
    templateReport.mainContent = templateReport.mainContent.replace(
        /{tableBody}/, tableBody);
    var uri = 'data:application/vnd.ms-excel;base64,';
    download(templateReport.mainContent, filename + ".xls", "application/vnd.ms-excel");
}
    
function replaceTemplate(template, data) {
    var result = template;
    listProperty = Object.keys(data);
    listProperty.forEach(property => result = result.replace("{" + property + "}", data[property] ? data[property] : "&nbsp;"));
    return result;
}

var listMonths = [];
for(var i = 0; i < 12;i++) {
    listMonths.push({
        value: i + 1
    });
}

var listYears = [];
for(var i = new Date().getFullYear() + 1; i > new Date().getFullYear() - 5 ;i--) {
    listYears.push({
        value: i
    });
}

function toNumber(strNumber){
    var numStr = strNumber;
    if (null == strNumber || '' == strNumber) {
        return 0;
    }
    
    while(numStr.indexOf('.') != -1) {
        numStr = numStr.replace('.', '');
    }
    while(numStr.indexOf(',') != -1) {
        numStr = numStr.replace(',', '.');
    }
    if(numStr.indexOf('(') != -1) {
        numStr = numStr.replace('(', '');
        numStr = numStr.replace(')', '');
        numStr = '-' + numStr;
    }
    return parseFloat(numStr);
}

function formatNumberFromStr(yourNumber,numberRound) {
    return (+yourNumber).toLocaleString('vi', {maximumFractionDigits:numberRound, useGrouping:true});
}

function formatNumberFromStrNew(yourNumber,numberRound) {
    if(yourNumber==null || yourNumber == '') return 0;
    var components = yourNumber.toString().replace(/\./g, ',').split(",");
    if(components[0]==""||components[0]==" "){
        components[0]=0;
    }
    if(components[0]=="-"){
    components[0]='-0';
    }
    if (components.length === 1)
        components[0] = yourNumber;
    components[0] = components[0].toString().replace(/(?!-)[^0-9.]/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    if (components.length === 2)
        components[1] = components[1].toString().replace(/(?!-)[^0-9.]/g, "").substr(0, numberRound);
    var numStr = components.join(",").substr(0,1)=='-'?'(' +components.join(",").substr(1) + ')':components.join(",");
    return numStr;
}

function validateLicensePlates(element, event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode < 48 || (57 < charCode && charCode < 65) ||
        (90 < charCode && charCode < 97) || charCode > 122) return false;
    var length = element.value.length;
    if (length >= 8) return false;
    if (length != 2 && 57 < charCode) return false;
    if (length == 2 && (charCode >= 48 && 57 >= charCode)) return false;
    return true;
}

var mucViPhams = [];
mucViPhams.push({
    key: "0",
    value: "Tất cả"
});
mucViPhams.push({
    key: "1",
    value: "Lớn hơn 15 phút"
});
mucViPhams.push({
    key: "2",
    value: "Lớn hơn 30 phút"
});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    if(cvalue == "" || cvalue == null) {
        document.cookie = cname + "=;" + expires + ";path=/";
    } else {
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    var counti = ca.length;
    for (var i = 0; i < counti; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function getParamTuNgay_DenNgay(year, month, currentDate) {
    var objDate = {};
    objDate.tuNgay = moment([year, month - 1]).startOf('month').format("DD/MM/YYYY");
    var denNgay = moment([year, month - 1]).endOf('month').format("DD/MM/YYYY");
    var a = moment(objDate.tuNgay, "DD/MM/YYYY").valueOf();
    var b = moment(denNgay, "DD/MM/YYYY").valueOf();
    if(a <= moment(currentDate, "DD/MM/YYYY").valueOf() && b >= moment(currentDate, "DD/MM/YYYY").valueOf()) {
        objDate.denNgay = currentDate;
    } else {
        objDate.denNgay = denNgay;
    }
    return objDate;
}

function Html2Excel(idName, fileName, sheetName) {
    $('#' + idName+ ' td').attr({
      "data-f-name": "Times New Roman",
      "data-a-v": "middle",
      "data-a-wrap": "true"
    });
    $('#' + idName+ ' .text-bold td').attr({
      "data-f-bold": "true"
    });
    $('#' + idName+ ' .text-red td').attr({
      "data-f-color": "FF0000"
    });
    $('#' + idName+ ' .text-center').attr({
      "data-a-h": "center"
    });
    $('#' + idName+ ' .text-right').attr({
      "data-a-h": "right"
    });
    $('#' + idName+ ' .text-size-11 td').attr({
      "data-f-sz": "11"
    });
    $('#' + idName+ ' .text-size-12 td').attr({
      "data-f-sz": "12"
    });
    $('#' + idName+ ' .text-size-16 td').attr({
      "data-f-sz": "16"
    });
    $('#' + idName+ ' .text-border td').attr({
      "data-b-a-s": "thin",
      "data-b-a-c": "A9A9A9"
    });
    TableToExcel.convert(document.getElementById(idName), {
      name: fileName + ".xlsx",
      sheet: {
        name: sheetName
      }
    });
}

function Html2Pdf(idName, fileName, widthArr, headerRows) {
    $('.tableExport th').css({
        "font-family": "times-bold"
    });
    $('#' + idName + ' td').css({
        "font-family": "times-normal"
    });
    $('#' + idName + ' .text-center').css({
        "text-align": "center"
    });
    $('#' + idName + ' .text-right').css({
        "text-align": "right"
    });
    $('#' + idName + ' .text-left').css({
        "text-align": "left"
    });
    $('#' + idName + ' .text-red td').css({
        "color": "red"
    });
    var htmlContent = $('#' + idName).html();
    var val = htmlToPdfmake(htmlContent);
    val[0].layout = "noBorders";
    val[0].table.widths = widthArr;
    val[1].table.widths = widthArr;
    val[1].table.body[0].forEach(td => td.fillColor = "#FFFFFF");
    val[1].table.body[1].forEach(td => td.fillColor = "#FFFFFF");
    val[1].table.headerRows = headerRows ? headerRows : 1;
    val[1].table.dontBreakRows = true;
    console.log(val);
    var dd = {
        footer: function(currentPage, pageCount) {
            return {text: currentPage.toString() + ' / ' + pageCount, alignment: 'center', fontSize: 10}
        },
        info: {
            title: fileName
        },
        pageSize: { width: 841.89, height: 595.28},
        pageOrientation: 'landscape',
        pageMargins: [15, 15, 30, 23],
        content: val
    };
    pdfMake.createPdf(dd).open();
    pdfMake.createPdf(dd).download(
        fileName + ".pdf");
}

//format select2 with searching + scroll infinite
function formatSelect2(id, data) {
    $("#" + id).select2({
        data: data,
        containerCssClass: 'form-control',
        query: function(q) {
            var pageSize = 10,
                results = this.data.filter(function(e) {
                    return new RegExp(q.term, "i").test(e.text);
                });
            var paged = results.slice((q.page - 1) *
                pageSize, q.page * pageSize);
            q.callback({
                results: paged,
                more: results.length >= q.page *
                    pageSize
            });
        }
    });
}
