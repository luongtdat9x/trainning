function replacer(key, value) {
    //    console.log(typeof value);
    if (key === "$$hashKey") {
        return undefined;
    } else if (typeof value === 'number') {
        if (value != null) {
            return value + "";
        }
    } else if (key == 'fieldErr') {
        return;
    }
    if (value == "null1") {
        return;
    }

    return value;
}

function replacevalue(value, str) {
    var re = new RegExp(str, 'g');
    value = value.replace(re, '');

    return value;
}

app.service("BaseServices", function($http, $log, $q, $rootScope) {
    this.callAPI = function(url, method, model) {
        model = replacevalue(JSON.stringify(model, replacer), 'fieldErr:');
        url = url + "&h=" + hash(model);
        var def = $q.defer(),
            req = {
                method: method,
                dataType: 'json',
                contentType: "application/json;charset=utf-8",
                url: '/GSHT/' + url
            };

        if (!method) {
            req.method = 'GET';
        }
        if (model) {
            req.data = model;
        }
        $http(req).success(function(data) {
            def.resolve(data);
        }).error(function(data, code) {
            def.reject(data);
            if (data.actionError) {
                $rootScope.$emit('data-error', {
                    msg: data.actionError
                });
            }
            $log.error(data, code);
        });
        return def.promise;
    }

    this.callMultiAPI = function(url, method, model) {
        model = JSON.stringify(model, replacer);
        var deferred = $q.defer();
        var urlCalls = [];
        angular.forEach(urls, function(url) {
            urlCalls.push($http.get(url.url));
        });
        // they may, in fact, all be done, but this
        // executes the callbacks in then, once they are
        // completely finished.
        $q.all(urlCalls)
            .then(
                function(data) {
                    deferred.resolve(data)
                },
                function(errors) {
                    deferred.reject(errors);
                },
                function(updates) {
                    deferred.update(updates);
                });
        return deferred.promise;
    }
});