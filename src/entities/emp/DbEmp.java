package entities.emp;

public class DbEmp {
	private String stt;
	private String manv;
	private String tennv;
	private String maphongban;
	private String tenphongban;
	private String congviec;
	private String luong;
	private String ngaytao;
	private String trangthai;
	public String getStt() {
		return stt;
	}
	public void setStt(String stt) {
		this.stt = stt;
	}
	public String getManv() {
		return manv;
	}
	public void setManv(String manv) {
		this.manv = manv;
	}
	public String getTennv() {
		return tennv;
	}
	public void setTennv(String tennv) {
		this.tennv = tennv;
	}
	public String getMaphongban() {
		return maphongban;
	}
	public void setMaphongban(String maphongban) {
		this.maphongban = maphongban;
	}
	public String getTenphongban() {
		return tenphongban;
	}
	public void setTenphongban(String tenphongban) {
		this.tenphongban = tenphongban;
	}
	public String getCongviec() {
		return congviec;
	}
	public void setCongviec(String congviec) {
		this.congviec = congviec;
	}
	public String getLuong() {
		return luong;
	}
	public void setLuong(String luong) {
		this.luong = luong;
	}
	public String getNgaytao() {
		return ngaytao;
	}
	public void setNgaytao(String ngaytao) {
		this.ngaytao = ngaytao;
	}
	public String getTrangthai() {
		return trangthai;
	}
	public void setTrangthai(String trangthai) {
		this.trangthai = trangthai;
	}
	public DbEmp() {
		super();
	}
	public DbEmp(String stt, String manv, String tennv, String maphongban, String tenphongban, String congviec,
			String luong, String ngaytao, String trangthai) {
		super();
		this.stt = stt;
		this.manv = manv;
		this.tennv = tennv;
		this.maphongban = maphongban;
		this.tenphongban = tenphongban;
		this.congviec = congviec;
		this.luong = luong;
		this.ngaytao = ngaytao;
		this.trangthai = trangthai;
	}


}
