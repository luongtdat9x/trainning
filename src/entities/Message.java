package entities;

public class Message {
    public Message() {

    }

    public Message(String errCode, String errMessage) {
        this.errCode = errCode;
        this.errMessage = errMessage;
    }

    public Message(String errCode, String errMessage, Object object) {
        this.errCode = errCode;
        this.errMessage = errMessage;
        this.object = object;
    }

    private String errCode;
    private String errMessage;
    private Object object;

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public Object getObject() {
        return object;
    }
}

