package action.user;


import com.opensymphony.xwork2.ActionSupport;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;

import common.AppConstants;


public class LoginAction extends ActionSupport implements SessionAware,
                                                          ServletRequestAware,
                                                          ServletResponseAware {
    private static final long serialVersionUID = 1L;
    private transient HttpServletRequest request;
    private transient HttpServletResponse response;
    private Map<String, Object> sessionMap;
    private static final Logger logger = Logger.getLogger(LoginAction.class);

    public String execute() {
        response.setHeader("X-Frame-Options", "SAMEORIGIN");
        response.setHeader("X-XSS-Protection", "1; mode=block");
        response.setHeader("X-Content-Type-Options", "nosniff");
        response.setHeader("Strict-Transport-Security", "max-age=31536000");
        response.setHeader("Referrer-Policy", "origin-when-cross-origin");
        response.setHeader("Expect-CT", "max-age=7776000, enforce");
        response.setContentType("text/xml; charset=utf-8; gzip");
        return SUCCESS;
    }

    public String checkLogin() throws Exception {
    	logger.info("inside LoginAction execute method");
        response.setHeader("X-Frame-Options", "SAMEORIGIN");
        response.setHeader("X-XSS-Protection", "1; mode=block");
        response.setHeader("X-Content-Type-Options", "nosniff");
        response.setHeader("Strict-Transport-Security", "max-age=31536000");
        response.setHeader("Referrer-Policy", "origin-when-cross-origin");
        response.setHeader("Expect-CT", "max-age=7776000, enforce");
        response.setContentType("text/xml; charset=utf-8; gzip");
        try {
            HttpSession session = request.getSession();
            if (request.getParameter("query") != null) {
                return ActionSupport.ERROR;
            }
            if (session.getAttribute(AppConstants.APP_USER_ID_SESSION) !=
                null) {
                return SUCCESS;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            addActionError(ex.getMessage());
            return ActionSupport.ERROR;
        } finally {
            
        }

        return ActionSupport.SUCCESS;
    }

    public void setSession(Map<String, Object> sessionMap) {
        this.sessionMap = sessionMap;
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setServletResponse(HttpServletResponse response) {
        this.response = response;
    }
}
