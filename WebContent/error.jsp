<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/fmt.tld" prefix="fmt"%>
<jsp:include page="/includes/tpcp_header.jsp"></jsp:include>
<div id="content">
 <div>
  <div class="panel-heading border-bottom">
   <h1 class="panel-title">
    <strong></strong>
   </h1>
  </div>  
  <div class="panel panel-default" style="height:300px;">
        <div class="app_error" style="margin-top:40px;">
         <s:if test="hasActionErrors()">
             <div class="app_error_list">
               <font size="4" color="Red"><b> C&#7843;nh b&#225;o:</b> </font><s:actionerror/>
              </div>
          </s:if>
        </div>
  </div>
 </div>
</div>
<%@ include file="/includes/tpcp_bottom.jsp"%>