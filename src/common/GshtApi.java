package common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import java.util.Map;

import org.apache.commons.io.IOUtils;


public class GshtApi {

    public String callApi(String strUrl, String strSubUrl, Map<?, ?> params) throws MalformedURLException,
                                                                                    IOException {
        try {
            if (strUrl == null || strUrl.isEmpty()) {
                throw new RuntimeException("Failed : Invalid url");
            }
            strUrl = AppConstants.URL_API_MASTER + strUrl;
            if (strSubUrl != null && !strSubUrl.isEmpty()) {
                strUrl = strUrl + "/" + strSubUrl;
            }
            if (params != null && !params.isEmpty()) {
                strUrl = strUrl + "?" + urlEncodeUTF8(params);
            }
            URL url = new URL(strUrl);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json;charset=UTF-8");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            StringReader stringReader = new StringReader(br.readLine());
            String strJson = IOUtils.toString(stringReader);
            return strJson;
        } catch (RuntimeException ex) {
            throw ex;
        } catch (MalformedURLException ex) {
            throw ex;
        } catch (IOException ex) {
            throw ex;
        }
    }

    public String callApi(String strUrl, String strSubUrl) throws MalformedURLException, IOException {
        return callApi(strUrl, strSubUrl, null);
    }

    public String callApi(String strUrl, Map<?, ?> params) throws MalformedURLException, IOException {
        return callApi(strUrl, null, params);
    }

    public String callApi(String strUrl) throws MalformedURLException, IOException {
        return callApi(strUrl, null, null);
    }

    private String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    private String urlEncodeUTF8(Map<?, ?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s", urlEncodeUTF8(entry.getKey().toString()),
                                    urlEncodeUTF8(entry.getValue().toString())));
        }
        return sb.toString();
    }

}
