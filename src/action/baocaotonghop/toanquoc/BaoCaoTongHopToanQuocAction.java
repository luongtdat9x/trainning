package action.baocaotonghop.toanquoc;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import action.AppAction;
import common.AppConstants;
import common.GshtApi;
import entities.Message;

public class BaoCaoTongHopToanQuocAction extends AppAction {

    private transient Map<String, Object> mapParam;

    public BaoCaoTongHopToanQuocAction() {
        super();
    }

    public String executeAction(HttpServletRequest request,
                                HttpServletResponse response) throws Exception {
        return SUCCESS;
    }

    public String view(HttpServletRequest request,
                       HttpServletResponse response) throws Exception {
        try {
            msg = new Message();
            String strUrl = AppConstants.URL_TONG_HOP_TOAN_QUOC;
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("start", mapParam.get("tuNgay"));
            params.put("end", mapParam.get("denNgay"));
            params.put("vehicleType", mapParam.get("vehicleType"));
            GshtApi gshtApi = new GshtApi();
            String strJson = gshtApi.callApi(strUrl, params);
            msg.setObject(strJson);
            msg.setErrCode("0");
            msg.setErrMessage("Truy vấn dữ liệu thành công!");
            return SUCCESS;
        } catch (Exception ex) {
            msg.setErrCode("1");
            msg.setErrMessage("Truy vấn dữ liệu thất bại!");
            throw ex;
        }
    }
    
    public String process(HttpServletRequest request,
                       HttpServletResponse response) throws Exception {
        try {
            msg = new Message();
            String strUrl = AppConstants.URL_TONG_HOP_TOAN_QUOC;
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("start", mapParam.get("tuNgay"));
            params.put("end", mapParam.get("denNgay"));
            params.put("vehicleType", mapParam.get("vehicleType"));
            GshtApi gshtApi = new GshtApi();
            String strJson = gshtApi.callApi(strUrl, params);
            msg.setObject(strJson);
            msg.setErrCode("0");
            msg.setErrMessage("Truy vấn dữ liệu thành công!");
            return SUCCESS;
        } catch (Exception ex) {
            msg.setErrCode("1");
            msg.setErrMessage("Truy vấn dữ liệu thất bại!");
            throw ex;
        }
    }

    public void setMapParam(Map<String, Object> mapParam) {
        this.mapParam = mapParam;
    }

    public Map<String, Object> getMapParam() {
        return mapParam;
    }
}
