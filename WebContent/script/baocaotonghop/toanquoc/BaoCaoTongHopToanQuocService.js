'use strict';
app.service('BaoCaoTongHopToanQuocService', ['BaseServices', function (BaseServices) {
var urls = {
        getView : "bcthtoanquocagl?action=view",
        getFile : "bcthtoanquocagl?action=process"
    }
function getView(data) {
      return BaseServices.callAPI(urls.getView, 'POST', data);
    }
function getFile(data) {
      return BaseServices.callAPI(urls.getFile, 'POST', data);
    }
var service = {
        getView : getView,
        getFile : getFile
    };return service;
}
]);