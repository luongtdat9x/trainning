package delegate.emp;

import java.util.Collection;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import dao.emp.EmpDAONew;

public class EmpDelegateNew {
    private SqlSession session;

    public EmpDelegateNew(SqlSession session) {
        this.session = session;
    }

    public Collection<?> getListEmp(Map<String, Object> params) throws Exception {
    	EmpDAONew kqDao = new EmpDAONew(session);
        return kqDao.getListEmp(params);
    }
}
